<!--
SPDX-FileCopyrightText: 2024 Jan Staněk <jstanek@redhat.com>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# rust-batch-update – Automated update of interconnected rust RPMs

Author knows how to use this package.
Everyone else should avoid it.
