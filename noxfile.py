# SPDX-FileCopyrightText: 2024 Jan Staněk <jstanek@redhat.com>
# # SPDX-License-Identifier: AGPL-3.0-or-later

import nox


@nox.session
def test(session: nox.Session) -> None:
    session.install(".[dev]")
    session.run("python", "-m", "pytest")
