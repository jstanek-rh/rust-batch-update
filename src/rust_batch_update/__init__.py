# SPDX-FileCopyrightText: 2024 Jan Staněk <jstanek@redhat.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import importlib.metadata

try:
    __version__ = importlib.metadata.version(__package__)
except importlib.metadata.PackageNotFoundError:
    __version__ = "0.0.0-UNKNOWN"
