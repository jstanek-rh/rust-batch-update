# SPDX-FileCopyrightText: 2024 Jan Staněk <jstanek@redhat.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import os
import typing as t
from pathlib import Path

import click
import click_log

from . import plan
from . import types as tp
from .config import ACTIVE_FEDORAS, STATE_FILE, USUAL_SUSPECTS

LOGGER: t.Final = click_log.basic_config(__package__)


class ChainListParam(click.ParamType):
    """List of values in chain-build format"""

    name = "chain-build-list"

    def convert(
        self,
        value: t.Any,
        param: click.Parameter | None,
        ctx: click.Context | None,
    ) -> tp.BuildOrder:
        if isinstance(value, (tp.SequentialBuildOrder, tp.ParallelBuildOrder)):
            return value

        try:
            groups = t.cast(str, value).strip().split(":")
            if len(groups) == 1:
                return tp.SequentialBuildOrder(groups[0].strip().split())
            else:
                return tp.ParallelBuildOrder(g.strip().split() for g in groups)

        except AttributeError:
            self.fail(
                f"{value!r} cannot be split into chain-build sequence!",
                param,
                ctx,
            )


class CommaListParam(click.ParamType):
    """Comma-separated list of values"""

    name = "comma-list"

    def convert(
        self,
        value: t.Any,
        param: click.Parameter | None,
        ctx: click.Context | None,
    ) -> list[str]:
        if isinstance(value, list):
            return value

        try:
            return [item.strip() for item in value.split(",")]
        except AttributeError:
            self.fail(f"{value!r} cannot be split by comma!", param, ctx)


@click.command()
@click_log.simple_verbosity_option(LOGGER)
@click.help_option()
@click.version_option()
@click.option(
    "--crates",
    "-c",
    type=ChainListParam(),
    default=USUAL_SUSPECTS,
    help="The crates to update",
)
@click.option(
    "-C",
    "cwd",
    type=click.Path(
        exists=True,
        file_okay=False,
        resolve_path=True,
        path_type=Path,
    ),
    default=Path.cwd(),
    help="Change working directory before updating",
)
@click.option(
    "--releases",
    "-r",
    type=CommaListParam(),
    default=ACTIVE_FEDORAS,
    help="Comma-separated list of Fedora releases to update",
)
@click.option(
    "--resume/--no-resume",
    default=False,
    help="Attempt (or not) resuming previously failed run",
)
@click.option(
    "--state-file",
    type=click.Path(dir_okay=False, resolve_path=True, path_type=Path),
    default=STATE_FILE,
    help="JSON file for persisting state after failed run",
)
@click.argument("patchver")
def main(
    crates: tp.BuildOrder,
    cwd: Path,
    releases: list[tp.FedoraBranch],
    resume: bool,
    state_file: Path,
    patchver: str,
) -> None:
    """Update all crates to PATCHVER."""

    if resume:
        try:
            batch = plan.Batch.load_saved(state_file)
        except FileNotFoundError as err:
            msg = f"State file {state_file!s} does not exist; cannot resume!"
            raise click.UsageError(msg) from err
    else:
        batch = plan.Batch.from_build_order(crates, releases)

    LOGGER.debug("Changing working directory to %s.", cwd)
    os.chdir(cwd)

    try:
        batch.make_commits(patchver)
        batch.chain_build()
    except Exception:
        batch.save(state_file)
        raise
    else:
        state_file.unlink(missing_ok=True)


main()
