# SPDX-FileCopyrightText: 2024 Jan Staněk <jstanek@redhat.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Configuration-like constants"""

import os
from collections.abc import Sequence
from pathlib import Path
from typing import Final

from . import types as tp

ACTIVE_FEDORAS: Final[Sequence[tp.FedoraBranch]] = [
    "rawhide",
    "f41",
    "f40",
    "f39",
    "epel9",
]

STATE_FILE: Final = Path(
    os.getenv("XDG_STATE_HOME", f"{os.environ["HOME"]}/.local/state"),
    __package__,
    "state.json",
)

#: The usual packages this is intended to update
USUAL_SUSPECTS: Final = tp.ParallelBuildOrder(
    [
        {"cxx-gen", "cxx-build", "cxxbridge-macro", "cxxbridge-flags"},
        {"cxx"},
    ]
)
