# SPDX-FileCopyrightText: 2024 Jan Staněk <jstanek@redhat.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Predefined operations on packages"""

import contextlib as ctx
import logging
import re
import shlex
import shutil
import subprocess
import sys
import typing as t
from collections import abc, deque
from itertools import chain, filterfalse
from operator import methodcaller
from pathlib import Path
from typing import Any, Final, Sequence

import attrs
import click

from . import types as tp
from .config import ACTIVE_FEDORAS

SIDE_TAG_RE: Final = re.compile(r"\w+-build-side-\d+")
KOJI_TASK_RE: Final = re.compile(r"^Created task: (?P<id>\d+)$")

LOGGER: Final = logging.getLogger(__name__)


def consume(it: t.Iterator[t.Any]) -> None:
    """Consume whole iterator, discarding values."""

    deque(it, maxlen=0)


def shellout(
    command: str | Sequence[str], **run_kwargs: Any
) -> subprocess.CompletedProcess[str]:
    """Shell out to system to do an operation.

    `command` is optionally split to separate arguments via the `shlex` module.

    Additional keyword arguments are passed to `subprocess.run`.
    """

    if isinstance(command, str):
        command = shlex.split(command)

    run_kwargs.setdefault("check", True)
    run_kwargs.setdefault("text", True)

    LOGGER.debug("Running command: $ %s", shlex.join(command))
    return subprocess.run(command, **run_kwargs)


def scan_file_object(
    source: t.IO[str], pattern: re.Pattern[str], *, sink: t.IO[str] | None = None
) -> abc.Generator[re.Match[str], None, None]:
    """Copy contents of source to sink (if any) while searching for pattern.

    Yields:
        First found match of a pattern.
    """

    for line in source:
        if sink is not None:
            sink.write(line)
        if match := pattern.search(line):
            yield match
            break
    else:
        raise RuntimeError(f"Pattern r'{pattern.pattern}' not found while scanning!")

    if sink is not None:
        shutil.copyfileobj(source, sink)


def request_side_tag(release: tp.FedoraBranch) -> tp.TagName:
    """Request side tag in koji."""

    command = ["fedpkg", "--release", release, "request_side_tag"]
    LOGGER.debug("Running command: $ %s", shlex.join(command))
    with subprocess.Popen(command, stdout=subprocess.PIPE, text=True) as request:
        assert request.stdout is not None

        scan = scan_file_object(request.stdout, SIDE_TAG_RE, sink=sys.stdout)
        tag_name = next(scan).group()
        consume(scan)

    return tag_name


@attrs.frozen
class Repo:
    """Dist-git repository on disk"""

    #: Path to repository root
    root: Path = attrs.field(converter=Path)

    #: directory stack
    _chdir: ctx.chdir[Path] = attrs.field(init=False)

    @_chdir.default
    def _default_chdir(self) -> ctx.chdir[Path]:
        return ctx.chdir(self.root)

    def query_current_version(self, specfile: str | Path | None = None) -> str:
        """Query current version from a spec file.

        Arguments:
            - specfile: Path to spec file to query. When given:

                `str`
                    Assumes file name relative to `self.root`.

                `Path`
                    Assumes full path to the spec file.

                `None`
                    Constructs spec file name from `self.root` base name.

        Returns: First version found in the spec file.
        """

        if specfile is None:
            specfile = self.root / f"{self.root.name}.spec"
        elif isinstance(specfile, str):
            specfile = self.root / specfile

        LOGGER.debug("Querying spec file '%s' for version", specfile)
        query = (
            "rpm",
            "--query",
            "--specfile",
            str(specfile),
            "--queryformat",
            r"%{version}\n",
        )
        result = shellout(query, capture_output=True)
        version = result.stdout.splitlines()[0].strip()
        LOGGER.debug("Detected version: %s", version)
        return version

    def __enter__(self) -> "EnteredRepo":
        """Change current working directory to `self.root`."""

        LOGGER.debug("Entering directory '%s'", self.root)
        self._chdir.__enter__()
        return EnteredRepo(root=Path.cwd())

    def __exit__(self, *excinfo: Any) -> None:
        LOGGER.debug("Leaving directory '%s'", self.root)
        self._chdir.__exit__(*excinfo)


@attrs.frozen
class EnteredRepo(Repo):
    """Namespace for operations relying on $PWD being the repo root dir."""

    def clean_state(self) -> None:
        """Bring the repository to a clean state:

        * Removes all untracked and ignored files,
            resets all modifications.
        * Fetches status of all remotes.
        * Switches the current branch to 'rawhide'.
        * Fast-forward any changes from upstream.
        """

        LOGGER.getChild("clean_state").info(
            "Cleaning repository %s",
            self.root,
        )
        shellout("git clean -fdx")
        shellout("git fetch --all")
        shellout("git checkout rawhide")
        shellout("git merge --ff-only @{upstream}")

    def generate_specfile(
        self,
        version: str,
        crate: str | None = None,
    ) -> None:
        """Regenerate spec file using rust2rpm."""

        logger = LOGGER.getChild("generate_specfile")

        if crate is None:
            crate = self.root.name.removeprefix("rust-")
        logger.info("Generating spec file for crate %s", crate)

        command = ["rust2rpm", "--store-crate"]
        if any(self.root.glob("*-fix-metadata.diff")):
            command += ["--patch"]
        command += [crate, version]

        shellout(command)

    def upload_sources(self, glob: str = "*.crate") -> None:
        """Upload sources to the lookaside cache."""

        logger = LOGGER.getChild("upload_sources")

        sources = tuple(map(str, self.root.glob(glob)))
        assert len(sources) == 1
        logger.info("Uploading new sources %s", sources[0])
        shellout(("fedpkg", "new-sources") + (sources))

    def commit(
        self,
        new_version: str,
        bzids: frozenset[tp.BugzillaId] | None = None,
        *,
        interactive: bool = True,
    ) -> tp.Commit:
        """Prepare commit and it's message.

        If interactive, will ask user for missing data and confirmations.
        """

        def mkmarkers(ids: frozenset[tp.BugzillaId]) -> list[str]:
            """Format marker lines for commit message."""

            numeric = filter(methodcaller("isnumeric"), ids)
            nummarks = map("rhbz#{}".format, sorted(numeric, key=int))
            numline = ", ".join(nummarks)

            aliases = filterfalse(methodcaller("isnumeric"), ids)
            aliasline = ", ".join(sorted(aliases))

            return list(filter(None, chain(numline, aliasline)))

        logger = LOGGER.getChild("prepare_commit")

        shellout("git add .")

        if interactive:
            shellout("git diff --staged")
            click.confirm("Apply changes", prompt_suffix="? ", abort=True)

        if interactive and bzids is None:
            prompt = click.prompt("Bugzillas (space-separated)", type=str)
            bzids = frozenset(prompt.split())

        message_parts = [f"Update to version {new_version}"]
        if bzids:
            message_parts.append(f"({", ".join(mkmarkers(bzids))})")
        message = " ".join(message_parts)

        logger.info("Committing changes with message '%s'", message)
        command = [
            "git",
            "commit",
            "--edit" if interactive else "--no-edit",
            "--message",
            message,
        ]
        shellout(command)

        return tp.Commit(
            hash=shellout("git rev-parse HEAD", capture_output=True).stdout.strip(),
            bugzillas=frozenset() if bzids is None else bzids,
        )

    def push_changes(self, target_seq: Sequence[str] = ACTIVE_FEDORAS) -> None:
        """Push changes to all targets, cherry-picking if needed."""

        logger = LOGGER.getChild("push_changes")

        atomic_push = "git", "push", "--atomic", "origin"
        target_map = tuple(map("rawhide:{}".format, target_seq))

        logger.info("Trying atomic push to %s", target_map)
        try:
            shellout(atomic_push + target_map)
        except subprocess.CalledProcessError:
            logger.info(
                "Atomic push failed; pushing separate branches"
            )  # continue below
        else:
            return  # successful atomic push

        for branch in target_seq:
            shellout(f"git checkout '{branch}'")

            logger.info("On branch '%s': Merging upstream", branch)
            shellout("git merge --ff-only @{upstream}")
            if branch != "rawhide":
                shellout("git cherry-pick --ff rawhide")

            logger.info("On branch '%s': Pushing", branch)
            shellout("git push")

    def chain_build(
        self,
        dependency_chain: tp.BuildOrder,
        *,
        release: tp.FedoraBranch,
        target: tp.TagName,
    ) -> abc.Generator[int, None, None]:
        """Chain-build this RPM with others in chain."""

        logger = LOGGER.getChild("chain_build")

        command = ["fedpkg", "--release", release, "chain-build", "--target", target]
        command += dependency_chain.argv

        logger.debug("Running command: $ %s", shlex.join(command))
        with subprocess.Popen(command, stdout=subprocess.PIPE, text=True) as build:
            assert build.stdout is not None
            scan = scan_file_object(build.stdout, KOJI_TASK_RE, sink=sys.stdout)
            yield int(next(scan).group("id"))
            consume(scan)
