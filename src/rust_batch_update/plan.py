# SPDX-FileCopyrightText: 2024 Jan Staněk <jstanek@redhat.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""High-level work organization"""

import json
import logging
import typing as t
from collections import abc
from pathlib import Path

import attrs
from cattrs.preconf.json import make_converter as make_json_converter
from cattrs.strategies import configure_tagged_union

from . import operation as op
from . import types as tp

LOGGER = logging.getLogger(__name__)


JSON: t.Final = make_json_converter()
configure_tagged_union(tp.BuildOrder, JSON)


@attrs.frozen
class Crate:
    """Metadata for single updated crate"""

    name: tp.CrateName
    commit: tp.Commit | None = None
    build: tp.BuildId | None = None
    update: abc.MutableMapping[tp.FedoraBranch, tp.UpdateName] | None = None

    @property
    def rpm_name(self) -> str:
        return f"rust-{self.name}"

    def committed(
        self, new_patchver: str, *, releases: t.Sequence[tp.FedoraBranch]
    ) -> t.Self:
        """Update to new patchver, commit, and push to dist-git."""

        with op.Repo(self.rpm_name) as repo:
            repo.clean_state()

            current_version = repo.query_current_version()
            major_minor, dot, _patch = current_version.rpartition(".")
            new_version = "".join((major_minor, dot, new_patchver))

            repo.generate_specfile(new_version, crate=self.name)
            repo.upload_sources()

            commit = repo.commit(new_version)
            repo.push_changes(releases)
            return attrs.evolve(self, commit=commit)


@attrs.frozen
class Batch:
    """State of the whole batch update"""

    #: All involved crates indexed by name
    crate_map: abc.MutableMapping[tp.CrateName, Crate]
    #: Chain build order, either sequential or parallel within a group
    build_order: tp.BuildOrder
    #: Build side-tag
    build_tag_map: abc.MutableMapping[tp.FedoraBranch, tp.TagName | None]

    @classmethod
    def from_build_order(
        cls, build_order: tp.BuildOrder, target_releases: t.Sequence[tp.FedoraBranch]
    ) -> t.Self:
        """Initialize new batch from crate build order and target Fedora releases."""

        crate_map = {name: Crate(name) for name in build_order.items()}

        build_tag_map: dict[str, str | None] = {
            release: None for release in target_releases
        }

        return cls(crate_map, build_order, build_tag_map)

    @classmethod
    def load_saved(cls, save_path: Path) -> t.Self:
        with save_path.open("r") as save:
            unstructured = json.load(save)
        return JSON.structure(unstructured, cls)

    def save(self, save_path: Path) -> None:
        save_path.parent.mkdir(parents=True, exist_ok=True)
        unstructured = JSON.unstructure(self)
        with save_path.open("w") as save:
            json.dump(unstructured, save)

    def crates_without_commit(self) -> t.Iterator[Crate]:
        return filter(lambda c: c.commit is None, self.crate_map.values())

    def make_commits(self, patchver: str) -> None:
        """Make update commits for all crates that need it."""

        for crate in self.crates_without_commit():
            self.crate_map[crate.name] = crate.committed(
                patchver, releases=list(self.build_tag_map.keys())
            )

    def chain_build(self) -> None:
        """Chain-build all committed crates in side-tag."""

        logger = LOGGER.getChild("chain_build")

        committed = self.build_order.select(
            lambda name: self.crate_map[name].commit is not None
        )

        rpm_order, final_rpm = committed.map(
            lambda name: self.crate_map[name].rpm_name
        ).partition_final()

        logger.debug("Selected %s as final chain-build RPM", final_rpm)

        for release, side_tag in self.build_tag_map.items():
            if side_tag is None:
                logger.debug("Requesting side tag for %s", release)
                side_tag = self.build_tag_map[release] = op.request_side_tag(release)

            with op.Repo(final_rpm) as build_repo:
                # prepare build
                build = build_repo.chain_build(
                    rpm_order, release=release, target=side_tag
                )

                # start and retrieve main task ID
                task_id = build.send(None)
                for crate in map(self.crate_map.__getitem__, committed.items()):
                    self.crate_map[crate.name] = attrs.evolve(crate, build=task_id)

                # wait for the build to finish
                op.consume(build)
