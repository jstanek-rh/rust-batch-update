# SPDX-FileCopyrightText: 2024 Jan Staněk <jstanek@redhat.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""Custom and descriptive types"""

from __future__ import annotations

import typing as t
from collections import abc
from itertools import chain, islice

import attrs

# Conceptual named types
CrateName = str  #: Name of a rust crate

# Dist-git related named types
FedoraBranch = str  #: Fedora branch/relase: rawhide, f40, epel9, …
BugzillaId = str  # Bug identifier/alias in bugzilla: 12345, CVE-2024-00001, …

# Build-related named types
BuildId = int  #: koji build ID
TagName = str  #: koji side-tag name

# Update-related named types
UpdateName = str  #: bodhi update name


@attrs.frozen
class Commit:
    """Commit information"""

    hash: str
    bugzillas: frozenset[BugzillaId]


def _convert_item_order(data: abc.Iterable[str]) -> tuple[str, ...]:
    return tuple(data)


def _convert_group_order(
    data: abc.Iterable[abc.Iterable[str]],
) -> tuple[set[str], ...]:
    return tuple(set(group) for group in data)


@attrs.frozen
class SequentialBuildOrder:
    """Chain build order without parallel groups"""

    item_order: abc.Sequence[str] = attrs.field(converter=_convert_item_order)

    @property
    def argv(self) -> abc.Iterable[str]:
        """Argument vector for ``fedpkg chain-build``"""

        return self.item_order

    def items(self) -> abc.Iterator[str]:
        """Flattened iterator over current items"""

        return iter(self.item_order)

    def select(self, callable: t.Callable[[str], bool]) -> SequentialBuildOrder:
        """Select some items while preserving order."""

        return self.__class__(filter(callable, self.item_order))

    def map(self, callable: t.Callable[[str], str]) -> SequentialBuildOrder:
        """Convert all items."""

        return self.__class__(map(callable, self.item_order))

    def partition_final(self) -> tuple[SequentialBuildOrder, str]:
        """Partition the final item from self."""

        try:
            *head, final = self.item_order
        except ValueError as no_final_item:
            raise ValueError(f"No final item in {self.item_order}") from no_final_item

        return self.__class__(head), final


@attrs.frozen
class ParallelBuildOrder:
    """Chain build order with parallel groups"""

    group_order: abc.Sequence[abc.Set[str]] = attrs.field(
        converter=_convert_group_order
    )

    @property
    def argv(self) -> abc.Iterable[str]:
        """Argument vector for ``fedpkg chain-build``"""

        group_iter = iter(self.group_order)
        yield from next(group_iter, set())
        for group in group_iter:
            yield ":"
            yield from group

    def items(self) -> abc.Iterator[str]:
        """Flattened iterator over current items"""

        for group in self.group_order:
            yield from group

    def select(self, callable: t.Callable[[str], bool]) -> ParallelBuildOrder:
        """Select some items while preserving order."""

        return self.__class__(filter(callable, group) for group in self.group_order)

    def map(self, callable: t.Callable[[str], str]) -> ParallelBuildOrder:
        """Convert all items."""

        return self.__class__(map(callable, group) for group in self.group_order)

    def partition_final(self) -> tuple[ParallelBuildOrder, str]:
        """Partition the final item from self."""

        try:
            last_group = set(self.group_order[-1])
        except IndexError as no_final_group:
            raise ValueError(
                f"No final group in {self.group_order}"
            ) from no_final_group

        try:
            final = last_group.pop()
        except KeyError as no_final_item:
            raise ValueError(f"No final item in {self.group_order}") from no_final_item

        return self.__class__(chain(islice(self.group_order, -1), [last_group])), final


BuildOrder = SequentialBuildOrder | ParallelBuildOrder
